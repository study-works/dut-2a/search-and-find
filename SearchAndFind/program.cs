﻿using System;
using System.Threading;
using Urho;
using Urho.Gui;
using Urho.Shapes;
using Urho.SharpReality;
using Windows.ApplicationModel.Core;


namespace SearchAndFind
{
    internal class Program
    {
        [MTAThread]
        static void Main() => CoreApplication.Run(new UrhoAppViewSource<SearchAndFindApp>());
    }

    public class SearchAndFindApp : StereoApplication
    {

        //define the different states of the app
        private enum Mode
        {
            MAPPING,
            PLACE,
            STARTGAME,
            INGAME
        };

        //the number of sphere that the gamemaster will place
        private const int NBSPHERE = 3;

        //the numero of the sphere you are placing or searching
        private int sphereAct =0;

        //thecolors of the spheres you will place
        private Color[] lesCouleurs = new Color[NBSPHERE];

        private bool wireframe;

        //A node to contain the surfaces scanned
        private Node environmentNode;

        //A node to contain the placed shere
        private Node sphereNode;

        //A Node to contain the text we will display to the user, it will follow user's look
        private Node textNode;

        //A Node to contain the text we will display to the user, it will follow user's look
        private Node indicatorNode;

        //A Node to contain the sphere that the gamemaster will place, it will follow user's look
        private Node placeNode;

        private Material spatialMaterial;

        private Text3D text3d;

        //The base value of the score you will gain when you find a sphere
        private const int SCORE_SPHERE = 10000;

        //Your score, initialize at 0
        private int score = 0;

        //The timer wich will update the factor in real time
        private Timer timer;

        //The factor which will be used to claculate the score
        private ScoreMultiplier factor;

        private Mode mode =Mode.MAPPING;

        public SearchAndFindApp(ApplicationOptions opts) : base(opts) { }

        protected override async void Start()
        {
            base.Start();

            Zone.AmbientColor = new Color(0.2f, 0.2f, 0.2f);
            DirectionalLight.Brightness = 0.5f;
            spatialMaterial = new Material();
            spatialMaterial.SetTechnique(0, CoreAssets.Techniques.NoTextureUnlitVCol, 1, 1);
            EnableGestureTapped = true;


            environmentNode = Scene.CreateChild();
            sphereNode = Scene.CreateChild();
            indicatorNode = Scene.CreateChild();


            //Create a TextField in front of the user
            //This TextField will be used to dispaly text temporarily to give some informations to the user
            textNode = Scene.CreateChild();
            textNode.Position = ((RightCamera.GetScreenRay(0.5f,0.5f).Origin+ LeftCamera.GetScreenRay(0.5f, 0.5f).Origin)*0.5f+RightCamera.GetScreenRay(0.5f, 0.5f).Direction + LeftCamera.GetScreenRay(0.5f, 0.5f).Direction)*0.5f;
            text3d = textNode.CreateComponent<Text3D>();
            textNode.Rotation = RightCamera.Node.Rotation;
            textNode.SetScale(0.2f);
            text3d.HorizontalAlignment = HorizontalAlignment.Center;
            text3d.VerticalAlignment = VerticalAlignment.Center;
            text3d.SetColor(Color.White);
            text3d.SetFont(CoreAssets.Fonts.AnonymousPro, 26);
            //text3d.Text = "Test";

            //Initialise the colors of the spheres that the user will place
            lesCouleurs[0] = Color.Blue;
            lesCouleurs[1] = Color.Yellow;
            lesCouleurs[2] = Color.Magenta;



            // Start the spatial mapping
            var test = await StartSpatialMapping(new Vector3(50, 50, 10), 1200);


        }

        protected override void OnUpdate(float timeStep)
        {
            /* update the position of the indicator sphere to make it follow the look of the gamemaster
             * based on the looking direction of the gamemaster, we do 2 rotations to locate the sphere in the bottom right hand corner of his field of view.
             */
            Ray cameraRay = RightCamera.GetScreenRay(0.5f, 0.5f);
            textNode.Position = cameraRay.Origin + cameraRay.Direction;
            textNode.Rotation = RightCamera.Node.Rotation;
            double angleV = 0.145, angleH = 0.25;
            Vector3 tmp = cameraRay.Direction;
            if (tmp.Z < 0) angleV = -angleV;
            Vector3 tmp2 = new Vector3((float)(tmp.X * Math.Cos(angleH) + tmp.Z * Math.Sin(angleH)), (float)tmp.Y, (float)(tmp.X * (-Math.Sin(angleH)) + tmp.Z * Math.Cos(angleH)));
            Vector3 u = new Vector3(1, 0, (-tmp2.X) / tmp2.Z);
            u.Normalize();
            Vector3 tmp3 = cameraRay.Origin + rotate(tmp2, u, angleV);
            //if(tmp3.Y>tmp.Y)
            //    tmp3 = cameraRay.Origin + rotate(tmp2, u, -angle);
            indicatorNode.Position = tmp3;


            //if we are in place mode, update the position of the sphere to place to make it follow the look of the gamemaster
            if (mode == Mode.PLACE)
            {
                var result = Scene.GetComponent<Octree>().RaycastSingle(cameraRay, RayQueryLevel.Triangle, 100, DrawableFlags.Geometry, 0x70000000);
                if (result != null)
                {
                    placeNode.Position = result.Value.Position;
                }
            }
        }


        /*
         * rotate vector3 around the axe generated by the vector unit by an angle given in radians using rotation matrix
         */
        private Vector3 rotate(Vector3 vector3, Vector3 unit, double angle)
        {
            float xf, yf, zf, ux, uy, uz, x, y, z;
            float c, s;
            c = (float)Math.Cos(angle);
            s = (float)Math.Sin(angle);
            x = vector3.X;
            y = vector3.Y;
            z = vector3.Z;
            ux = unit.X;
            uy = unit.Y;
            uz = unit.Z;
            xf = x * (ux * ux * (1 - c) + c) + y * (ux * uy * (1 - c) - uz * s) + z * (ux * uz * (1 - c) + uy * s);
            yf = x * (ux * uy * (1 - c) + uz * s) + y * (uy * uy * (1 - c) + c) + z * (uy * uz * (1 - c) - ux * s);
            zf = x * (ux * uz * (1 - c) - uy * s) + y * (uy * uz * (1 - c) + ux * s) + z * (uz * uz * (1 - c) + c);
            return new Vector3(xf, yf, zf);
        }


        /*
         * Our app work in 3 step, first you need to scan the environment.
         * Then the gamemaster will place the spheres, put the game in a waiting state and give the holens to the player.
         * The the player can start the game when he is ready and search and pick up the sheres in the right order,
         * the sphere to pick is indicated in a corner of his field of view.
         */
        public override void OnGestureTapped()
        {
            Sphere sphere;
            switch (mode) {
                case Mode.MAPPING:
                    //Stop the scan
                    StopSpatialMapping();

                    //Create the first sphere ,that the user will place, in front of him
                    placeNode = Scene.CreateChild();
                    placeNode.Position = new Vector3(0.3f, 0.15f, 0.2f);
                    placeNode.SetScale(0.05f);
                    sphere = placeNode.CreateComponent<Sphere>();
                    sphere.Color = lesCouleurs[0];
                    sphere.ViewMask = 0x80000000;


                    mode = Mode.PLACE;
                    break;
                case Mode.PLACE:
                    //Clone the sphere to let it at the place chosen by the gamemaster and remove it from the place node
                    Node tmp = sphereNode.CreateChild();
                    tmp.SetScale(0.05f);
                    tmp.Position = placeNode.Position;
                    Sphere sphere1 =(Sphere) tmp.CloneComponent(placeNode.GetComponent<Sphere>());
                    sphere1.ViewMask = 4294967295;
                    placeNode.RemoveAllComponents();
                    //if there is still spheres to place create the next sphere in front on the gamemaster
                    if (sphereAct < NBSPHERE - 1)
                    {
                        sphereAct++;
                        sphere = placeNode.CreateComponent<Sphere>();
                        sphere.Color = lesCouleurs[sphereAct];
                        sphere.ViewMask = 0x80000000;

                    }
                    //if there is no sphere left to place remove the spatial mapping result to let only the spheres and go to the waiting state
                    else
                    {
                        Scene.RemoveChild(environmentNode);
                        mode = Mode.STARTGAME;
                    }
                    break;
                case Mode.STARTGAME:
                    //notify the player that the game have started
                    sphereAct = 0;
                    popText("Game Start", 3000);
                    mode = Mode.INGAME;

                    //Start the timer wich will upadate the score factor
                    var autoEvent = new AutoResetEvent(false);
                    factor = new ScoreMultiplier(1, 0.9f);
                    timer = new Timer(factor.update,autoEvent, new TimeSpan(2,0,0),new TimeSpan(0,2,0));


                    //Create a sphere that will be use to indicate wich color the user need to find
                    sphere = indicatorNode.CreateComponent<Sphere>();
                    indicatorNode.SetScale(0.05f);
                    sphere.Color = sphereNode.GetChild((uint)sphereAct).GetComponent<Sphere>().Color;
                    indicatorNode.Position = new Vector3(0, 0, 1);
                    break;

                case Mode.INGAME:
                    /*
                     * Get the element in front of the player and compare it with the sphere search.
                     * If it match remove it from the Node,
                     * Pass to the next sphere, change the color of th indicator and add points to the score
                     * When you pick up the last sphere, stop the timer and show the score
                     */
                    if (sphereAct < NBSPHERE)
                    {
                        Ray cameraRay = RightCamera.GetScreenRay(0.5f, 0.5f);
                        var result = Scene.GetComponent<Octree>().RaycastSingle(cameraRay, RayQueryLevel.Triangle, 100, DrawableFlags.Geometry, 0x70000000);
                        if (result != null)
                        {
                            Node tmp2 = sphereNode.GetChild((uint)sphereAct);
                            if (result.Value.Node == tmp2)
                            {
                                sphere = indicatorNode.GetComponent<Sphere>();
                                sphere.Color = sphereNode.GetChild((uint)sphereAct).GetComponent<Sphere>().Color;
                                sphereNode.RemoveChild(sphereNode.GetChild((uint)sphereAct));
                                score += (int)factor.getFactor() * SCORE_SPHERE;
                                sphereAct++;
                                if(sphereAct== NBSPHERE)
                                {
                                    timer.Dispose();
                                    popText("Congratulation, your score is " + score, 5000);
                                }
                            }
                        }
                    }
                    break;
            }
        }


        //Show the String text to the user during a certain time given in milliseconds
        private void popText(String text, int time)
        {
            if (time != 0)
            {
                new Thread(() =>
                {
                    text3d.Text = text;
                    Thread.Sleep(time);
                    text3d.Text = "";
                }
                ).Start();
            }
        }

        /*
         * When the scan detecte new surfaces, we add them to the environnement
         */
        public override void OnSurfaceAddedOrUpdated(SpatialMeshInfo surface, Model generatedModel)
        {
            bool isNew = false;
            StaticModel staticModel = null;
            Node node = environmentNode.GetChild(surface.SurfaceId, false);
            if (node != null)
            {
                isNew = false;
                staticModel = node.GetComponent<StaticModel>();
            }
            else
            {
                isNew = true;
                node = environmentNode.CreateChild(surface.SurfaceId);
                staticModel = node.CreateComponent<StaticModel>();
            }

            node.Position = surface.BoundsCenter;
            //node.Rotation = surface.BoundsRotation;
            staticModel.Model = generatedModel;

            Material mat;
            Color startColor;
            Color endColor = new Color(0.8f, 0.8f, 0.8f);

            if (isNew)
            {
                startColor = Color.Blue;
                mat = Material.FromColor(endColor);
                staticModel.SetMaterial(mat);
            }
            else
            {
                startColor = Color.Red;
                mat = staticModel.GetMaterial(0);
            }

            mat.FillMode = wireframe ? FillMode.Wireframe : FillMode.Solid;
            var specColorAnimation = new ValueAnimation();
            specColorAnimation.SetKeyFrame(0.0f, startColor);
            specColorAnimation.SetKeyFrame(1.5f, endColor);
            mat.SetShaderParameterAnimation("MatDiffColor", specColorAnimation, WrapMode.Once, 1.0f);

        }
    }
}
