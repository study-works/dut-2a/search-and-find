# Search and Find - Class diagram

We have created an enumeration to make the make the method onGestureTapped able to have different actions. We chose to use one gesture tap because it is easy to detect so you cannot do mistakes.

The ScoreMultiplier  is used to manage how many points you will gain from picking a sphere based on the SCORE_SPHERE constant. By i=using a timer we reduce this points gradually during the game.

We have create a method popText so we can show a text on a Text3D in front of user's eyes and make it disappear after a given period of time. We used this method to notify the user when the state of the game change or to show to him what he must do to play.

The rotate method will rotate a vector around an axis by using a rotation matrix.

