# Search and Find - Feedback

This is a note with our feedbacks on the difficulties encountered.


### The development
In general, we didn't really have any difficulty. The only problem is that there isn't much documentation on hololens except the examples for UrhoSharp. This made us grope around a lot. But in the end everything went well for what we were able to do during "The Before".

### Finally
In the end, it was a project that we found very interesting, as it was different from what we had done so far. It opened new horizons for us.
