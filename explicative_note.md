# Search and Find - Explicative note

This is an explicative note with a documentation and explanations for our choices.

## Documentation

## Explanations
We have choose the UrhoSharp technology. In effect, we also could have used Unity. But UrhoSharp is realy simple to use and is more than enough for what we want to do.\
The advantage of UrhoSharp is that we program everything ourselves directly in the code, which is not the case with Unity (and it's not funny...).\
In addition, for the design, we can do a lot with little code and all in a single file.
